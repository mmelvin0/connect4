package main

import (
	"gitlab.com/mmelvin0/connect4/pkg/connect4"
)

func main() {
	connect4.Run()
}
