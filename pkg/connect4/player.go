package connect4

import "math/rand"

type Player struct {
	Name   string    `json:"name"`
	Secret string    `json:"-"`
	Turn   GameState `json:"-"`
}

func (p *Player) Is(name string) bool {
	return p != nil && p.Name == name
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func newSecret() string {
	runes := make([]rune, 10)
	for i := range runes {
		runes[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(runes)
}
