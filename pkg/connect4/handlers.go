package connect4

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"strconv"
)

func PlayGame(c *Context) (int, string, error) {
	c.Lock()
	defer c.Unlock()
	secret := c.Auth()
	game, player := c.Server.Authorize(secret)
	if player == nil {
		if secret == "" {
			return 401, "Authorization required!\n", nil
		}
		return 403, "Invalid authorization. Try joining a game?\n", nil
	}
	c.Text()
	if game.IsOver() {
		return 403, fmt.Sprintf("Game over. %s won!\n", game.Winner.Name), nil
	}
	if !game.IsTurn(player) {
		return 409, "Not your turn!\n", nil
	}
	column, err := strconv.Atoi(c.Param("column"))
	if err != nil {
		return 422, "Invalid column\n", err
	}
	if !game.Place(column) {
		return 409, "Full column\n", err
	}
	return 200, "Placed!\n", nil
}

func GetState(c *Context) (int, string, error) {
	c.Lock()
	defer c.Unlock()
	buffer, err := json.Marshal(c.Server)
	if err != nil {
		return 500, "", err
	}
	c.JSON()
	return 200, string(buffer), nil
}

func JoinGame(c *Context) (int, string, error) {
	c.Lock()
	defer c.Unlock()
	c.Text()
	name := c.Param("player")
	if len(name) < 2 {
		return 422, "Player name too short\n", nil
	}
	if len(name) > MaxPlayerName {
		return 422, "Player name too long\n", nil
	}
	if c.Server.HasPlayer(name) {
		return 403, fmt.Sprintf("%s is already playing\n", name), nil
	}
	player := &Player{Name: name, Secret: newSecret()}
	var joined *Game = nil
	for _, game := range c.Server.Games {
		if !game.IsOpen() {
			continue
		}
		if game.Join(player) {
			joined = game
			break
		}
	}
	if joined == nil {
		if c.Server.IsFull() {
			return 503, "Max game count reached\n", nil
		}
		joined = c.Server.AddGame()
		_ = joined.Join(player)
	}
	return 201, fmt.Sprintf("Game: %s\nPlayer: %s\nSecret: %s\n", joined.Name, player.Name, player.Secret), nil
}

func LeaveGame(c *Context) (int, string, error) {
	c.Lock()
	defer c.Unlock()
	secret := c.Auth()
	if secret == "" {
		return 401, "", nil
	}
	game, player := c.Server.Authorize(secret)
	if game == nil || player == nil {
		return 403, "", nil
	}
	game.Resign(player)
	return 410, "", nil
}

func GetGameCount(c *Context) (int, string, error) {
	c.Lock()
	defer c.Unlock()
	c.Text()
	return 200, string(len(c.Server.Games)), nil
}

func SetGameCount(c *Context) (int, string, error) {
	c.Lock()
	defer c.Unlock()
	if c.Auth() != AdminSecret {
		return 403, "", nil
	}
	c.Text()
	count, err := strconv.Atoi(mux.Vars(c.Request)["count"])
	if err != nil {
		return 422, "Game count must be an integer\n", err
	}
	if count < 0 || count > MaxGameCount {
		return 422, "Game count out of range\n", nil
	}
	c.Server.SetGameCount(count)
	return 200, fmt.Sprintf("There are now %d games\n", len(c.Server.Games)), nil
}
