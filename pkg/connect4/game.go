package connect4

import (
	"math/rand"
)

type Game struct {
	Name    string    `json:"name"`
	State   GameState `json:"state"`
	Board   Board     `json:"board"`
	Player1 *Player   `json:"player1"`
	Player2 *Player   `json:"player2"`
	Winner  *Player   `json:"winner"`
}

func (g *Game) Has(name string) bool {
	return g != nil && g.Player1.Is(name) || g.Player2.Is(name)
}

func (g *Game) Authorize(secret string) *Player {
	if secret == "" {
		return nil
	}
	if g.Player1 != nil && g.Player1.Secret == secret {
		return g.Player1
	}
	if g.Player2 != nil && g.Player2.Secret == secret {
		return g.Player2
	}
	return nil
}

func (g *Game) IsOpen() bool {
	return g.State == AwaitingPlayers
}

func (g *Game) Join(player *Player) bool {
	if g.Player1 == nil {
		g.Player1 = player
		player.Turn = Player1Turn
		return true
	}
	if g.Player2 == nil {
		g.Player2 = player
		player.Turn = Player2Turn
		g.State = Player1Turn
		return true
	}
	return false
}

func (g *Game) IsTurn(player *Player) bool {
	return g.State == player.Turn
}

func (g *Game) Resign(player *Player) {
	if g.Player1 == player {
		if g.Player2 == nil {
			g.Player1 = nil
			g.State = AwaitingPlayers
		} else {
			g.Winner = g.Player2
		}
	}
	if g.Player2 == player {
		if g.Player1 == nil {
			g.Player2 = nil
			g.State = AwaitingPlayers
		} else {
			g.Winner = g.Player1
		}
	}
}

func (g *Game) IsOver() bool {
	return g.Winner != nil
}

func (g *Game) Place(column int) bool {
	var (
		cell Cell
		next GameState
	)
	if g.State == Player1Turn {
		cell = Player1Cell
		next = Player2Turn
	} else if g.State == Player2Turn {
		cell = Player2Cell
		next = Player1Turn
	}
	position := g.Board.Place(column, cell)
	if position == nil {
		return false
	}
	winner := g.Won(*position)
	if winner != nil {
		g.Player1 = nil
		g.Player2 = nil
		g.Winner = winner
	} else {
		g.State = next
	}
	return true
}

func (g *Game) Won(position Position) *Player {
	cell := g.Board.Won(position)
	if cell == Player1Cell {
		return g.Player1
	}
	if cell == Player2Cell {
		return g.Player2
	}
	return nil
}

type GameState int

const (
	AwaitingPlayers GameState = iota
	Player1Turn
	Player2Turn
)

func NewGame() *Game {
	return &Game{
		Name:  NameList[rand.Intn(len(NameList))], // fixme: has been seen to produce duplicate names
		Board: NewBoard(),
		State: AwaitingPlayers,
	}
}
