package connect4

const BoardWidth = 7
const BoardHeight = 6
const InitialGameCount = 3
const AdminSecret = "eric"
const MaxPlayerName = 16
const MaxGameCount = 50
const WebServerPort = 47398
