package connect4

type Board []Column

type Column []Cell

type Cell int

type Position struct {
	Y int
	X int
}

const (
	EmptyCell Cell = iota
	Player1Cell
	Player2Cell
)

func (b Board) Place(x int, cell Cell) *Position {
	if x < 0 || x >= BoardWidth {
		return nil
	}
	for y := 0; y < BoardHeight; y++ {
		if b[y][x] != EmptyCell {
			return nil
		}
		if y+1 == BoardHeight || b[y+1][x] != EmptyCell {
			b[y][x] = cell
			return &Position{y, x}
		}
	}
	return nil
}

func (b Board) Won(p Position) Cell {
	// Check down
	for y := 0; y < BoardHeight-3; y++ {
		for x := 0; x < BoardWidth; x++ {
			if CheckLine(b[y][x], b[y+1][x], b[y+2][x], b[y+3][x]) {
				return b[y][x]
			}
		}
	}
	// Check right
	for y := 0; y < BoardHeight; y++ {
		for x := 0; x < BoardWidth-3; x++ {
			if CheckLine(b[y][x], b[y][x+1], b[y][x+2], b[y][x+3]) {
				return b[y][x]
			}
		}
	}
	// Check down-right
	for y := 0; y < BoardHeight-3; y++ {
		for x := 0; x < BoardWidth-3; x++ {
			if CheckLine(b[y][x], b[y+1][x+1], b[y+2][x+2], b[y+3][x+3]) {
				return b[y][x]
			}
		}
	}
	// Check down-left
	for y := BoardHeight - 3; y < BoardHeight; y++ {
		for x := 0; x < BoardWidth-3; x++ {
			if CheckLine(b[y][x], b[y-1][x+1], b[y-2][x+2], b[y-3][x+3]) {
				return b[y][x]
			}
		}
	}
	return EmptyCell
}

func CheckLine(a, b, c, d Cell) bool {
	return d != EmptyCell && a == b && a == c && a == d
}

func NewBoard() Board {
	board := make([]Column, BoardHeight)
	for y := 0; y < BoardHeight; y++ {
		board[y] = make([]Cell, BoardWidth)
		for x := 0; x < BoardWidth; x++ {
			board[y][x] = EmptyCell
		}
	}
	return board
}
