package connect4

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
)

type Action func(*Context) (int, string, error)

type ActionFactory struct {
	Action Action
	Server *Server
	Silent bool
}

func (h *ActionFactory) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c := &Context{
		Action:  h,
		Request: r,
		Server:  h.Server,
		Writer:  w,
	}
	if !h.Silent {
		Log("-> ... %s %s", r.Method, r.URL.String())
	}
	code, body, err := h.Action(c)
	if err != nil {
		Err(err)
	}
	w.WriteHeader(code)
	if body != "" {
		if _, err := w.Write([]byte(body)); err != nil {
			_, _ = fmt.Fprint(os.Stderr, err)
		}
	}
	if !h.Silent {
		Log("<- %d %s %s", code, r.Method, r.URL.String())
	}
}

type Context struct {
	Action  *ActionFactory
	Request *http.Request
	Server  *Server
	Writer  http.ResponseWriter
}

func (c *Context) Auth() string {
	return c.Request.Header.Get("Authorization")
}

func (c *Context) JSON() {
	c.Writer.Header().Set("Content-Type", "application/json")
}

func (c *Context) Text() {
	c.Writer.Header().Set("Content-Type", "text/plain")
}

func (c *Context) Param(name string) string {
	return mux.Vars(c.Request)[name]
}

func (c *Context) Lock() {
	c.Server.Mutex.Lock()
}

func (c *Context) Unlock() {
	c.Server.Mutex.Unlock()
}
