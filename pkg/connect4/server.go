package connect4

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"strings"
	"sync"
)

type Server struct {
	Games []*Game `json:"games"`
	Mutex sync.Mutex
}

func (s *Server) HasPlayer(name string) bool {
	for _, game := range s.Games {
		if game.Has(name) && !game.IsOver() {
			return true
		}
	}
	return false
}

func (s *Server) Authorize(secret string) (*Game, *Player) {
	if secret == "" {
		return nil, nil
	}
	for _, game := range s.Games {
		if player := game.Authorize(secret); player != nil {
			return game, player
		}
	}
	return nil, nil
}

func (s *Server) IsFull() bool {
	return len(s.Games) >= MaxGameCount
}

func (s *Server) SetGameCount(newCount int) {
	currentCount := len(s.Games)
	if newCount < currentCount {
		s.Games = s.Games[0:newCount]
	} else if newCount > currentCount {
		for i := currentCount; i < newCount; i++ {
			s.AddGame()
		}
	}
}

func (s *Server) AddGame() *Game {
	if len(s.Games) >= MaxGameCount {
		return nil
	}
	game := NewGame()
	s.Games = append(s.Games, game)
	return game
}

func (s *Server) FindGame(name string) *Game {
	for _, game := range s.Games {
		if game.Name == name {
			return game
		}
	}
	return nil
}

func Run() error {
	server := &Server{Games: make([]*Game, 0)}
	for i := 0; i < InitialGameCount; i++ {
		server.AddGame()
	}

	action := func(action Action) *ActionFactory {
		return &ActionFactory{Action: action, Server: server}
	}

	silent := func(af *ActionFactory) *ActionFactory {
		af.Silent = true
		return af
	}

	router := mux.NewRouter()
	api := router.PathPrefix("/api").Subrouter()

	api.Handle("/state", silent(action(GetState))).Methods("GET")
	api.Handle("/game/count", action(GetGameCount)).Methods("GET")
	api.Handle("/game/count/{count}", action(SetGameCount)).Methods("POST")
	api.Handle("/join/{player}", action(JoinGame)).Methods("POST")
	api.Handle("/leave", action(LeaveGame)).Methods("DELETE")
	api.Handle("/play/{column}", action(PlayGame)).Methods("POST")

	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("assets"))))

	webserver := http.Server{
		Addr:    fmt.Sprintf(":%d", WebServerPort),
		Handler: router,
	}
	return webserver.ListenAndServe()
}

func Err(err error) {
	_, _ = fmt.Fprintln(os.Stderr, err)
}

func Log(format string, args ...interface{}) {
	if !strings.HasSuffix(format, "\n") {
		format = fmt.Sprintf("%s\n", format)
	}
	_, _ = fmt.Fprintf(os.Stderr, format, args...)
}
