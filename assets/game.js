document.addEventListener('DOMContentLoaded', doTick);

// noinspection JSPotentiallyInvalidConstructorUsage
const dd = new diffDOM();

let lastUpdate;
const interval = 250; // milliseconds

async function doTick() {
    requestAnimationFrame(doTick);
    const now = performance.now();
    if (lastUpdate == null || (lastUpdate > -1 && now - lastUpdate > interval)) {
        lastUpdate = -1; // flag to prevent racing for first update
        try {
            await updateGame();
        } finally {
            lastUpdate = now;
        }
    }
}

async function updateGame() {
    const response = await fetch('api/state');
    const state = await response.json();
    const newRoot = document.createElement('div');
    newRoot.id = 'root';
    const ul = document.createElement('ul');
    ul.classList.add('games');
    newRoot.appendChild(ul);
    for (const game of state.games) {
        const li = document.createElement('li');
        const h2 = document.createElement('h2');
        const h3 = document.createElement('h3');
        const table = document.createElement('table');
        li.classList.add('game');
        table.classList.add('board');
        const trx = document.createElement('tr');
        for (let x = 0; x < game.board[0].length; x++) {
            const thx = document.createElement('th');
            thx.appendChild(document.createTextNode(x));
            trx.appendChild(thx);
        }
        table.appendChild(trx);
        for (let y = 0; y < game.board.length; y++) {
            const tr = document.createElement('tr');
            for (let x = 0; x < game.board[y].length; x++) {
                const td = document.createElement('td');
                td.classList.add('cell');
                switch (game.board[y][x]) {
                    case 0:
                        td.classList.add('empty');
                        break;
                    case 1:
                        td.classList.add('player1');
                        break;
                    case 2:
                        td.classList.add('player2');
                        break;
                }
                td.appendChild(document.createTextNode('●'));
                tr.appendChild(td);
            }
            table.appendChild(tr);
        }
        h2.appendChild(document.createTextNode(`Game: ${game.name}`));
        let status;
        if (game.winner) {
            status = `${game.winner.name} won!`;
        } else if (game.player1 || game.player2) {
            let p1 = game.player1 ? game.player1.name : '...';
            let p2 = game.player2 ? game.player2.name : '...';
            if (game.state === 1) {
                p1 = `<${p1}>`;
            } else if (game.state === 2) {
                p2 = `<${p2}>`;
            }
            status = `${p1} vs. ${p2}`;
        } else {
            status = 'awaiting players...';
        }
        h3.appendChild(document.createTextNode(status));
        li.appendChild(h2);
        li.appendChild(h3);
        li.appendChild(table);
        ul.appendChild(li);
    }
    const oldRoot = document.getElementById('root');
    dd.apply(oldRoot, dd.diff(oldRoot, newRoot));
}
